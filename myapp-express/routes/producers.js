var express = require('express');
var router = express.Router();

var producer_1 = {
  id: 1,
  name: 'Jean Martin',
  age: 32,
  sexe: 'male'
}; 

var producer_2 = {
  id: 2,
  name: "Jennifer Lattaux",
  age: 23,
  sexe: 'female'
}; 


var producers = [producer_1, producer_2];

/* GET producer listing. */
router.get('/', function(req, res, next) {
  res.send(producers);
});

/* GET producer listing. */
router.get('/:id', function(req, res, next) {
  var found=false;
  producers.forEach(prod => {
    if(prod.id==req.params.id){
      res.send(prod);
      found=true;
    }
  });
  
  if(!found){
    res.status(404)        // HTTP status 404: NotFound
      .send('Not found');
  }
 });


module.exports = router;
