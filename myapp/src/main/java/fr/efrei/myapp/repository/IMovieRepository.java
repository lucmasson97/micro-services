package fr.efrei.myapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import fr.efrei.myapp.model.Movie;

public interface IMovieRepository extends JpaRepository<Movie,Long>{

}
