package fr.efrei.myapp.services;

import fr.efrei.myapp.model.Movie;
import fr.efrei.myapp.model.MovieDTO;

public interface IProducerService {

	MovieDTO findProducer(Movie movie);
}
