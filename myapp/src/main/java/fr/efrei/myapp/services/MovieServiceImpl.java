package fr.efrei.myapp.services;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.efrei.myapp.model.Movie;
import fr.efrei.myapp.repository.IMovieRepository;

@Service
@Transactional(readOnly = true)
public class MovieServiceImpl implements IMovieService {

	@Autowired
	private IMovieRepository movieRepository;
	
	@Override
	public List<Movie> findAll() {
		return movieRepository.findAll();
	}

	@Override
	public Optional<Movie> findById(long id) {
		return movieRepository.findById(id);
	}

	@Override
	@Transactional
	public Movie update(Movie movie) {
		return movieRepository.findById(movie.getId()).map(m -> movieRepository.save(movie)).orElseThrow();

	}

	@Override
	@Transactional
	public Movie create(Movie movie) {
		return movieRepository.save(movie);
	}

	@Override
	@Transactional
	public void delete(long id) {
		movieRepository.findById(id).orElseThrow();
		movieRepository.deleteById(id);
	}
	

	@PostConstruct
	public void initDatabase() {
		Movie movie1= new Movie();
		movie1.setTitle("Star Wars 1");
		movie1.setProducer_id(1);
		
		Movie movie2= new Movie();
		movie2.setTitle("Star Wars 2");
		movie2.setProducer_id(2);

		movieRepository.saveAll(List.of(movie1, movie2));
	}

}
