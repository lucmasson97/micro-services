package fr.efrei.myapp.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.efrei.myapp.model.Movie;
import fr.efrei.myapp.model.MovieDTO;
import fr.efrei.myapp.model.Producer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProducerServiceImpl implements IProducerService{

	@Value("${url}")
	private  String URL_API;
	//private static String URL_API = "http://127.0.0.1:3000";

	@SuppressWarnings("rawtypes")
	private final CircuitBreakerFactory circuitBreakerFactory;

	public MovieDTO findProducer(Movie movie) {
		RestTemplate restTemplate = new RestTemplate();
		MovieDTO dto = convert(movie);
		log.info("Url called: "+URL_API);
		Producer producer = circuitBreakerFactory.create("findProducer")
				.run(() -> (Producer) restTemplate
						.getForObject("http://"+URL_API+"/producers/" + movie.getProducer_id(), Producer.class),
						t -> {
							log.error("findProducer call failed" + t);
							return new Producer("Unknown");
						});
		dto.setProducer(producer);
		return dto;
	}

	private MovieDTO convert(Movie movie) {
		MovieDTO dto = new MovieDTO();
		dto.setId(movie.getId());
		dto.setTitle(movie.getTitle());
		return dto;
	}

}
