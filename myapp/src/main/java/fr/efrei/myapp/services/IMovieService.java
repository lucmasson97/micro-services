package fr.efrei.myapp.services;

import java.util.List;
import java.util.Optional;

import fr.efrei.myapp.model.Movie;

public interface IMovieService {

	List<Movie> findAll();

	Optional<Movie> findById(long id);

	Movie update(Movie movie);

	Movie create(Movie movie);

	void delete(long id);

}
