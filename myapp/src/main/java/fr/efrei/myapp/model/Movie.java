package fr.efrei.myapp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Movie implements Serializable{

	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", producer_id=" + producer_id + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5733125123036658760L;
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	private String title;

	private long producer_id;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getProducer_id() {
		return producer_id;
	}

	public void setProducer_id(long producer_id) {
		this.producer_id = producer_id;
	}
}
