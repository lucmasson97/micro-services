package fr.efrei.myapp.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.efrei.myapp.model.Movie;
import fr.efrei.myapp.model.MovieDTO;
import fr.efrei.myapp.services.IMovieService;
import fr.efrei.myapp.services.IProducerService;

@RestController
@RequestMapping("/movies")
public class MovieController {

	@Autowired
	IMovieService movieManager;
	
	@Autowired
	IProducerService producerManager;
	
	@GetMapping
	public ResponseEntity<List<MovieDTO>> findAll() {
		List<Movie> movies = movieManager.findAll();
		List<MovieDTO> response = new ArrayList<MovieDTO>();

		movies.forEach(movie -> {
			response.add(producerManager.findProducer(movie));
		});

		return ResponseEntity.ok(response);
	}

	@GetMapping("/{id}")
	public ResponseEntity<MovieDTO> findById(@PathVariable long id) {

		return movieManager.findById(id).map(movie -> {
			return ResponseEntity.ok(producerManager.findProducer(movie));
		}).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping
	public ResponseEntity<Movie> createMovie(@RequestBody Movie request) {
		Movie movie = movieManager.create(request);
		if (movie.equals(movie)) {
			try {
				return ResponseEntity.created(new URI("/movies/" + movie.getId())).body(movie);
			} catch (URISyntaxException e) {
				e.printStackTrace();
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			return ResponseEntity.badRequest().build();
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Movie> updateMovie(@PathVariable long id, @RequestBody Movie movie) {
		movie.setId(id);
		return ResponseEntity.ok(movieManager.update(movie));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteMovie(@PathVariable long id) {
		movieManager.delete(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

}
